package com.example.tictactoegame

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_tic_tac_toe.*

class TicTacToeActivity : AppCompatActivity(), View.OnClickListener {

    private var firstPlayer: true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tic_tac_toe)
        click()
    }
    private fun click(){
        Button00.setOnClickListener{
            changePlayer(Button00)
        }
        Button01.setOnClickListener{
            changePlayer(Button01)
        }
        Button02.setOnClickListener{
            changePlayer(Button02)
        }
        Button10.setOnClickListener{
            changePlayer(Button10)
        }
        Button11.setOnClickListener{
            changePlayer(Button11)
        }
        Button12.setOnClickListener{
            changePlayer(Button12)
        }
        Button20.setOnClickListener{
            changePlayer(Button20)
        }
        Button21.setOnClickListener{
            changePlayer(Button21)
        }
        Button22.setOnClickListener{
            changePlayer(Button22)
        }
        resetButton.setOnClickListener {
            buttonContainer.visibility=View.VISIBLE
            TextView=""
            firstPlayer=true
            Button00.isClickable=true
            Button00.text=""
            Button01.isClickable=true
            Button01.text=""
            Button02.isClickable=true
            Button02.text=""
            Button10.isClickable=true
            Button10.text=""
            Button11.isClickable=true
            Button11.text=""
            Button12.isClickable=true
            Button12.text=""
            Button20.isClickable=true
            Button20.text=""
            Button21.isClickable=true
            Button21.text=""
            Button22.isClickable=true
            Button22.text=""
        }
    }
    private fun changePlayer(Button: Button){
        if (firstPlayer){
            Button.text="X"
        }else{
            Button.text="O"
        }
        Button.isClickable=false
        firstPlayer=false
        checkWinner()
    }
    private fun checkWinner(){
        if (Button00.text.isNotEmpty() && Button00.text.toString() == Button01.text.toString() && Button00.text.toString() == Button02.text.toString()) {
            TextView.text = "Winner is ${Button00.text}"
            buttonContainer.visibility=View.GONE
        } else if (Button00.text.isNotEmpty() && Button00.text.toString() == Button10.text.toString() && Button00.text.toString() == Button20.text.toString()) {
            Toast.makeText(this,"Winner is ${Button00.text}", Toast.LENGTH_SHORT).show()
            buttonContainer.visibility=View.GONE
        } else if (Button00.text.isNotEmpty() && Button00.text.toString() == Button11.text.toString() && Button00.text.toString() == Button22.text.toString()) {
            Toast.makeText(this,"Winner is ${Button00.text}", Toast.LENGTH_SHORT).show()
            buttonContainer.visibility=View.GONE
        } else if (Button01.text.isNotEmpty() && Button01.text.toString() == Button11.text.toString() && Button01.text.toString() == Button21.text.toString()) {
            Toast.makeText(this,"Winner is ${Button01.text}", Toast.LENGTH_SHORT).show()
            buttonContainer.visibility=View.GONE
        } else if (Button02.text.isNotEmpty() && Button02.text.toString() == Button12.text.toString() && Button02.text.toString() == Button22.text.toString()) {
            Toast.makeText(this,"Winner is ${Button02.text}", Toast.LENGTH_SHORT).show()
            buttonContainer.visibility=View.GONE
        } else if (Button02.text.isNotEmpty() && Button02.text.toString() == Button11.text.toString() && Button02.text.toString() == Button20.text.toString()) {
            Toast.makeText(this,"Winner is ${Button02.text}", Toast.LENGTH_SHORT).show()
            buttonContainer.visibility=View.GONE
        } else if (Button10.text.isNotEmpty() && Button10.text.toString() == Button11.text.toString() && Button10.text.toString() == Button12.text.toString()) {
            Toast.makeText(this,"Winner is ${Button10.text}", Toast.LENGTH_SHORT).show()
            buttonContainer.visibility=View.GONE
        } else if (Button20.text.isNotEmpty() && Button20.text.toString() == Button21.text.toString() && Button20.text.toString() == Button22.text.toString()) {
            Toast.makeText(this,"Winner is ${Button20.text}", Toast.LENGTH_SHORT).show()
            buttonContainer.visibility=View.GONE
        } else if (Button00.text.isNotEmpty() && Button01.text.isNotEmpty() && Button02.text.isNotEmpty() && Button10.text.isNotEmpty() && Button11.text.isNotEmpty()
            && Button12.text.isNotEmpty() && Button20.text.isNotEmpty() && Button21.text.isNotEmpty() && Button22.text.isNotEmpty()
    }
            TextView.text = "DRAW. No winner:("
            buttonContainer.visibility=View.GONE
    }

}